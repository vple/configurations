" see amix.dk/vim/vimrc.html

""""""""""
" => PLUGINS
""""""""""

"""
" PATHOGEN
" See: http://www.vim.org/scripts/script.php?script_id=2332
" See: https://github.com/tpope/vim-pathogen
"""
execute pathogen#infect()

"""
" IndentLine
"""
" Vertical line indentation
let g:indentLine_color_term = 239
let g:indentLine_char = '¦'

"""
" DelimitMate
"""
" Turn off expand on new line
let delimitMate_expand_cr = 1

"""
" NERDTree
"""
" Toggle file browsing pane
nmap <leader>d :NERDTreeToggle<CR>

"""
" CtrlP
"""
" Use <leader>t to open CtrlP
let g:ctrlp_map = '<leader>t'
" Ignore these directories
set wildignore+=*/build/**
" disable caching
let g:ctrlp_use_caching=0

"""
" Eclim
"""
" Disable compatible
set nocompatible
" filetype plugin on
filetype plugin indent on


"""
" Text, tab, indent
"""
" Use spaces instead of tabs
set expandtab

set shiftwidth=4
set tabstop=4

""""""""""
" => VIM User Interface
""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

"Always show current position
set ruler

"Height of command bar
set cmdheight=2

"Ignore case when searching
set ignorecase

"When searching try to be smart about cases
set smartcase

"Highlight search results
set hlsearch

"Makes search act like search in modern browsers
set incsearch

"Show matching brackets when text indicator is over them
set showmatch
"How many tenths of a second to blink when matching brackets
set mat=2

" Show line numbers
set number

""""""""""
" => Colors and Fonts
""""""""""
" Enable syntax highlighting
syntax enable

colorscheme desert
set background=dark

set encoding=utf8


""""""""""
" => Text, tabs, and indents
""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines


""""""""""
" => Moving around, tabs, windows and buffers
""""""""""
" Treat long lines as break lines (useful when moving aroudn in them)
map j gj
map k gk

" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
map <c-space> ?

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l


""""""""""
" => Status line
""""""""""
" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l:%c


""""""""""
" => Editing mappings
"""""""""
" Delete trailing white space on save
func! DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.java :call DeleteTrailingWS()
autocmd BufWrite *.html :call DeleteTrailingWS()


""""""""""
" => Helper functions
""""""""""
" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction
